import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { appservice } from '../appService';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  showButton= false;

public  employees:any = [];

  constructor( private appservice:appservice) {
    setTimeout(()=>{
      this.showButton=true;
    },5000);
    setTimeout(()=>{
      this.showButton=false;
    },10000)
   }

  ngOnInit() {
    
  }
  onGet(){
    this.appservice.getconfig().subscribe(
      (response)=> {this.employees=response
                    console.log(this.employees)},
      (error)=> console.log(error)

      )}

}
